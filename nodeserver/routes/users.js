const express= require('express');
const router= express.Router();
const passport=require('passport');
const jwt=require('jsonwebtoken');
var path = require('path');
var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const nodemailer = require("nodemailer");
var fs = require("fs");
var ejs = require("ejs");
const config  = require('../config/database');
const crud=require('../crud');
var async = require('async');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const  multipart  =  require('connect-multiparty');
const  multipartMiddleware  =  multipart({ uploadDir:  './uploads' });

router.post('/upload', multipartMiddleware, (req, res) => {
	console.log(req.files.uploads[0]['path']);
	console.log(req.files.uploads);
	
	var path=req.files.uploads[0]['path'].split("uploads/");/*Code for live*/
	 //var path=req.files.uploads[0]['path'].split("uploads\\");/*Code for local*/
	//console.log(path);
	//console.log(path[0]);
	// var path=req.files.uploads[0]['path'];
	str=`UPDATE users SET user_profileimg='${path[1]}' WHERE user_id='${req.body.user_id}'`;
	config.query(str, function (error, results) {
					  	if(!error)
					  	{
					  		// console.log(results);
					  		res.send(JSON.stringify(path[1]));
					  		// callback(null,results);
					  		// res.json({
						   //      'message': path[1]
						   //  });
					  	}
					  	else
					  	{
					  		// callback(error,null);
					  		// res.send("User add");
					  		console.log(error)
					  	}	
					});
	profile_percentage(req.body.user_id,'3')
	// res.send(req.files.uploads);
	// res.send(req.file.uploads);
 //    res.json({
 //        'message': req.files.uploads[0]['path']
 //    });
});


/*Register Routs*/
router.post('/register',(req,res,next)=>{

	crud.data1("count(*) as cnt","users",`user_email='${req.body.user_email}'`,function(error,result)
	{
		
		if(result[0].cnt > 1)
		{
			//res.status(400);
			//console.log(result[0].cnt);
			res.status(422).send("Duplicate User Found");
			//res.status(422);
		}	
		else
		{

			//res.send(JSON.stringify("Insert"));
					bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
						
					// Store hash in your password DB.
					if(!err){

						 crud.data2("users","user_name,user_email,user_pass,user_loginwith,user_verify",`'${req.body.fullname}','${req.body.user_email}','${hash}','website','No'`,function(error2,result2){
						 	
							  if(!error2)
							  {
								  //res.send(JSON.stringify("Login"));
								  crud.data1("user_id,user_name,user_email","users",`user_email='${req.body.user_email}'`,function(error1,rss)
									{
										// console.log(token);
										if(!error1)
										{
													let transporter = nodemailer.createTransport({
														name:"ns1.md-in-8.webhostbox.net",
														host: "mail.infinitechlabs.in",
															    port: 587,
															    secure: false, // true for 465, false for other ports
															    auth: {
															      user: "suraj@infinitechlabs.in", // generated ethereal user
															      pass: "Suraj@2015" // generated ethereal password
														},
															    tls:{
															    	rejectUnauthorized:false
															    }
													});
													var jsonPath = path.join(__dirname, '..', 'views\\');
													//console.log(jsonPath);
													fullname=req.body.fullname;
													email=req.body.user_email;
													ejs.renderFile(jsonPath+"registrationemail.ejs",{name:fullname,email:email},function(err,data){

														if(err)
														{
															console.log(err)
														}	
														else
														{
															let mailOptions = {
															from:'info@slected.me',
															to:email,
															subject:'Slected Me- Registration',
															//text:'This is test mail from nodemailer'
															//template:'verification'
															html:data
															}
															transporter.sendMail(mailOptions,function(err,info){
																if(err)
																{
																	console.log(err);
																}	
																else
																{
																	res.send(true);
																	//res.render('verification');
																	console.log(info.response);
																	
																}	
															});
															
														}	

													})
											token = jwt.sign({
										        data: {uid:rss[0].user_id,email:rss[0].user_email,status:1,fullname:rss[0].user_name}
										      }, 'hellowrold', { expiresIn: 60 });
											//console.log(token);
										    res.send(JSON.stringify(token))
											//res.send(result);
											
										}	
									});
							  }
							  else
							  {
							  	console.log(error2);
							  	//res.send(JSON.stringify("User not register."));
							  	//res.status(422).send("Invalid User");
							  }	
						  })  
					}
					else
					{
						 console.log(err);
					}	
				  });

		}	
			

	});
});

/*Send Verification Email*/
router.post('/verification_email',(req,res)=>{

	
	let transporter = nodemailer.createTransport({
		name:"ns1.md-in-8.webhostbox.net",
		host: "mail.infinitechlabs.in",
			    port: 587,
			    secure: false, // true for 465, false for other ports
			    auth: {
			      user: "suraj@infinitechlabs.in", // generated ethereal user
			      pass: "Suraj@2015" // generated ethereal password
		},
			    tls:{
			    	rejectUnauthorized:false
			    }
	});
var jsonPath = path.join(__dirname, '..', 'views\\');
//console.log(jsonPath);
fullname=req.body.user_name;
id=req.body.user_id;
email=req.body.user_email;
ejs.renderFile(jsonPath+"verification.ejs",{name:fullname,uid:id},function(err,data){

	if(err)
	{
		console.log(err)
	}	
	else
	{
		let mailOptions = {
		from:'info@slected.me',
		to:email,
		subject:'Slected Me- Email Verification ',
		//text:'This is test mail from nodemailer'
		//template:'verification'
		html:data
		}
		transporter.sendMail(mailOptions,function(err,info){
			if(err)
			{
				console.log(err);
			}	
			else
			{
				res.send(true);
				//res.render('verification');
				console.log(info.response);
				
			}	
		});
		
	}	

})
});

router.post('/verificationAction',function(req,res){
	var userid =req.body.userid;
	//console.log(userid);
	str=`UPDATE users SET user_verify='true' WHERE user_id='${userid}'`;
	config.query(str, function (error, results) {
					  	if(!error)
					  	{
					  		profile_percentage(userid,'2');
					  		// console.log(results);
					  		//res.send(results)
					  		
					  		// callback(null,results);
					  	}
					  	else
					  	{
					  		// callback(error,null);
					  		// res.send("User add");
					  		//console.log(error)
					  		res.send(error);
					  	}	
					}); 

	res.render('verificationAction');

})

/* Reset password email*/
router.post('/resetpass_email',(req,res)=>{

	
	let transporter = nodemailer.createTransport({
		name:"ns1.md-in-8.webhostbox.net",
		host: "mail.infinitechlabs.in",
			    port: 587,
			    secure: false, // true for 465, false for other ports
			    auth: {
			      user: "suraj@infinitechlabs.in", // generated ethereal user
			      pass: "Suraj@2015" // generated ethereal password
		},
			    tls:{
			    	rejectUnauthorized:false
			    }
	});
var jsonPath = path.join(__dirname, '..', 'views\\');
//console.log(jsonPath);
fullname=req.body.user_name;
os 		=req.body.OS;
browser =req.body.browser;
version =req.body.version; 
email =req.body.user_email;

id=req.body.user_id;

ejs.renderFile(jsonPath+"resetpass_email.ejs",{name:fullname,uid:id,os:os,browser:browser,browser_version:version},function(err,data){

	if(err)
	{
		console.log(err)
	}	
	else
	{
		let mailOptions = {
		from:'info@slected.me',
		to:email,
		subject:'Slected Me- Reset password ',
		//text:'This is test mail from nodemailer'
		//template:'verification'
		html:data
		}
		transporter.sendMail(mailOptions,function(err,info){
			if(err)
			{
				console.log(err);
			}	
			else
			{
				res.send(true);
				//res.render('verification');
				console.log(info.response);
			}	
		});
		
	}	

})
});

/* Check email exist or not*/
router.post('/checkEmail',(req,res,next)=>{
	crud.data1("count(*) as cnt,user_id,user_name,user_email","users",`user_email='${req.body.user_email}' `,function(error,result)
	{
		console.log(result[0].cnt);
		emailId=req.body.user_email;
		//console.log(emailId);
		if(!error)
		{
			//res.send(result);
			if(result[0].cnt > 0)
			{
				//token = {'uid':result[0].user_id,'email':result[0].user_email,'status':1,'fullname':result[0].user_name};
                //res.send(JSON.stringify(token));

                token = jwt.sign({
		        data: {uid:result[0].user_id,email:result[0].user_email,status_id:1,fullname:result[0].user_name}
		      }, 'hellowrold', { expiresIn: 60 });
			//console.log(token);
		    res.send(JSON.stringify(token))


				/*If user exist*/
				//res.send('1');
				
			}
			else
			{
				 token = jwt.sign({
		        data: {email:emailId,status_id:0}
		      }, 'hellowrold', { expiresIn: 60 });
			//console.log(token);
		    res.send(JSON.stringify(token))

				
				//token ={'email':emailId,'status':0};
                //res.send(JSON.stringify(token));
				/*If user not exist */
				//res.send('0');
			}
		}	
	});
});

/*Login Action*/
/*LOGIN ROUTES*/
router.post('/login',(req,res,next)=>{


	crud.data1("user_id,user_name,user_email,user_pass","users",`user_email='${req.body.email}' AND user_type='0'`,function(error,result)
	{
		//console.log(result.length);
		if(!error)
		{
			//res.send(result);
			if(result.length > 0)
			{
				bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
					// Store hash in your password DB.
					if(!err){
						dbpass = result[0].user_pass;
						userpass=req.body.password;
						console.log(userpass);
						bcrypt.compare(userpass, dbpass, function(err, result_from_crypt) {
							//res.send("1");
							console.log(result_from_crypt);
							// console.log(result_from_crypt);
							if(result_from_crypt==true)
							{
								token = jwt.sign({
		        						data: {uid:result[0].user_id,email:result[0].user_email,status:1,fullname:result[0].user_name}
		      							}, 'hellowrold', { expiresIn: 60 });			
								res.send(JSON.stringify(token));
							}
							else
							{
								res.status(401).send('Ungültiger Login');
							}
							
						});
					}
				  });
			}
			else
			{
				res.status(422).send("email does not exist");
			}
		}
		else
		{
			res.status(422).send(error);
		}	

	});
});

/*Admin Login*/
router.post('/adminlogin',(req,res,next)=>{


	crud.data1("user_id,user_name,user_email,user_pass","users",`user_email='${req.body.email}' AND user_type='1'`,function(error,result)
	{
		//console.log(result.length);
		if(!error)
		{
			//res.send(result);
			if(result.length > 0)
			{
				bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
					// Store hash in your password DB.
					if(!err){
						dbpass = result[0].user_pass;
						userpass=req.body.password;
						console.log(userpass);
						bcrypt.compare(userpass, dbpass, function(err, result_from_crypt) {
							//res.send("1");
							console.log(result_from_crypt);
							// console.log(result_from_crypt);
							if(result_from_crypt==true)
							{
								token = jwt.sign({
		        						data: {uid:result[0].user_id,email:result[0].user_email,status:1,fullname:result[0].user_name}
		      							}, 'hellowrold', { expiresIn: 60 });			
								res.send(JSON.stringify(token));
							}
							else
							{
								res.status(401).send('Ungültiger Login');
							}
							
						});
					}
				  });
			}
			else
			{
				res.status(422).send("email does not exist");
			}
		}
		else
		{
			res.status(422).send(error);
		}	

	});
});

/*RESET PASSWORD ROUTES*/
router.post('/resetPassword',(req,res,next)=>{
	id=req.body;
	bcrypt.hash(req.body.user_pass, saltRounds, function(err, hash) {
					// Store hash in your password DB.
					if(!err){
						 str=`UPDATE users SET user_pass='${hash}' WHERE user_id='${req.body.user_id}'`;
							config.query(str, function (error, results) {
											  	if(!error)
											  	{
											  		// console.log(results);
											  		res.send(results)
											  		// callback(null,results);
											  	}
											  	else
											  	{
											  		// callback(error,null);
											  		// res.send("User add");
											  		console.log(error)
											  	}	
											}); 

					}
				  });
});


router.post("/filter_cat_route",function(req,res,next){
	//console.log(req.body)
	id=req.body;
	//
	//console.log(id);
	crud.data1("*","product",`pro_catid='${id}'`,function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})
});
/*Add users skills*/
router.post('/adduserskill',(req,res)=>{
	id=req.body;
	console.log(req.body);
	//res.send("data added");
	req.body.candidateSkill.forEach(function(value){
	  crud.data1("count(*)as cnt","userskill",`userskill_userid='${value.user_id}'AND userskill_skillid='${value.candidateSkill}'`,function(error,result){
		//console.log("count"+error);
		if(!error)
		{
			if(result[0].cnt < 1)
			{
				// console.log(value);
			
				crud.data2("userskill","userskill_userid,userskill_skillid",`'${value.user_id}','${value.candidateSkill}'`,function(error1,result1){
					if(!error1)
					{
						
						res.status(200).send(JSON.stringify("Skills added Successfully"));
					}	
					else
					{
						console.log(error1);
						res.status(401).send(JSON.stringify("Skills not added Successfully"));
						
					}	
				});
			}
			else
			{
				
			}		
		}	
		else
		{
			//console.log(error);
			res.status(401).send(JSON.stringify("Something went wrong Please try again."));;
			//console.log(error);
		}	
	})
	});
//profile_percentage(req.body.user_id,'8');
	
	// res.send('Data added');
	//res.end("Data Added");
	// res.send(111);
	// res.send(JSON.parse())
	// Promise.all(result1)
	// .then((result) => res.send("success"))
	// .catch((err) => res.send(err));
});
/*Delete Users skills*/
router.post('/deleateusersSkill',(req,res)=>{
	uid=req.body.user_id
	userskill_skillid=req.body.candidateSkill
	//console.log(req.body);
	 crud.data4("userskill",`userskill_userid='${uid}'AND userskill_skillid='${userskill_skillid}'`,function(error,result){
		if(!error)
		{
			res.send(result);
		}	
		else
		{
			res.send(error);
		}	
	})

});

router.get('/compareData',(req,res,next)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body.user_id;
	//console.log(id);
	crud.data1("user_id,user_name,user_profileimg,rating_pro,rating_qual,rating_exp,rating_area,rating_avt,rating_salary,job_company,job_position","users,rating,job","user_id=rating_userid AND job_userid=user_id AND job_current='true'",function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	
		else
		{
			res.send(result);
		}	

	})
});

router.post('/FiltercompareData',(req,res)=>{
	//res.send('getSkill');
	
	area=req.body.rating_area;
	minRange=req.body.rating_Minrange;
	maxRange=req.body.rating_Maxrange;
	exp=req.body.rating_exp;
	skills=req.body.rating_skills;
	condition="";
	//console.log(req.body.rating_exp);

	if(typeof area !=='undefined')
	{
		condition+='AND rating_area LIKE '+`'${area}%'`;
	}	
	if(typeof minRange !=='undefined' && maxRange !=='undefined')
	{
		condition+=' AND rating_avt BETWEEN '+minRange+' AND '+maxRange;
	}	

	if(typeof exp !=='undefined' && exp !=='Erfahrung' && exp !=='Experience')
	{
		condition+=' AND rating_exp ='+exp
	}	

	if(typeof skills !='undefined' && skills !=='Skills')
	{
		condition+=` AND userskill_skillid='${req.body.rating_skills}'`
		//condition+=' AND userskill_skillid =`'${req.body.rating_skills}'`'
	}	
	// console.log(condition);
	crud.data1("user_id,user_name,user_profileimg,rating_pro,rating_qual,rating_exp,rating_area,rating_avt,rating_salary,job_company,job_position","users,rating,job,userskill",`user_id=rating_userid AND job_userid=user_id AND job_current='true' AND userskill_userid=user_id  `+condition+ ` GROUP BY user_id`,function(error,result){
		// console.log(result);
		if(!error)
		{
			res.send(result);
		}	
		else
		{
			res.send(result);
		}	

	})
});

router.post('/getuserskill',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body.user_id;
	//console.log(id);
	crud.data1("userskill_id,userskill_userid,userskill_skillid","userskill",`userskill_userid='${req.body.user_id}'`,function(error,result){
		//console.log(result);
		if(!error)
		{
			//res.send(result);
			res.send(JSON.stringify(result));
		}	

	})
});
router.post('/updatePersonalData',(req,res)=>{
	id=req.body;
	// console.log(req.body.user_dob);
	str=`UPDATE users SET user_name='${req.body.user_name}',user_email='${req.body.user_email}',user_bob='${req.body.user_dob}' WHERE user_id='${req.body.user_id}'`;
	config.query(str, function (error, results) {
					  	if(!error)
					  	{
					  		userid=req.body.user_id;
					  		profile_percentage(userid,'4');
							// console.log(results);
					  		res.send(results)
					  		// callback(null,results);
					  	}
					  	else
					  	{
					  		// callback(error,null);
					  		// res.send("User add");
					  		console.log(error)
					  	}	
					}); 

});

router.post('/imgPath',(req,res)=>{
	crud.data1("user_id,user_name,user_email,rating_avt,rating_pro,rating_qual,rating_exp,rating_salary,user_bob,user_verify,user_salary,user_profileimg","rating,users",`user_id=rating_userid AND user_id='${req.body.user_id}'`,function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})
});
router.post('/getuserdata',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	//console.log(id);
	crud.data1("user_id,user_name,user_email,rating_avt,rating_pro,rating_qual,rating_exp,rating_salary,user_bob,user_verify,user_salary,user_profileimg","rating,users",`user_id=rating_userid AND user_id='${req.body.user_id}'`,function(error,result){
		//console.log(result);

		if(!error)
		{
			res.send(result);
		}	

	})
});
router.post('/getjobdata',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	//id=req.body;
	//console.log(id);
	crud.data1("job_id,job_company,job_position,job_period_from,job_period_to,job_current","job",`job_userid='${req.body.user_id}' order by job_id desc`,function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})
});

/*Add users skills*/
router.post('/addUserJob',(req,res,next)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	//console.log(id);
	crud.data2("job","job_userid,job_company,job_position,job_period_from,job_period_to,job_current",`'${req.body.job_userid}','${req.body.job_company}','${req.body.job_position}','${req.body.job_period_from}','${req.body.job_period_to}','${req.body.job_current}'`,function(error,result){
		
			if(!error)
			{
							//res.send(result);
					  		userid=req.body.job_userid;
					  		profile_percentage(userid,'6');
			
				res.send(result);
			}	
	});

});

/*GET users skills*/
router.post('/usercurrentCompany',(req,res)=>{
	id=req.body;
	//console.log(id);
	crud.data1("job_id,job_company,job_position,job_period_from,job_period_to,job_current","job",`job_current='true' AND job_userid='${req.body.user_id}' ORDER BY job_id DESC LIMIT 1`,function(error,result){
		//console.log(result);
		if(!error)
		{	
			//console.log(result);
			if(result.length > 0)
			{
				res.send(result);	
			}	
			else
			{
				//console.log("Data not found");
				res.send("no");
			}
			
		}	

	})

});

/*Delete Previous Company*/
router.post('/deleateCompany',(req,res)=>{
	
	uid=req.body.job_userid
	job_id=req.body.job_id
	//console.log(req.body);
	 crud.data4("job",`job_userid='${uid}'AND job_id='${job_id}'`,function(error,result){
		if(!error)
		{
			res.send(result);
		}	
		else
		{
			res.send(error);
		}	
	})


});
router.post('/userprivousCompany',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	//res.send(req.body);
	//console.log(req.body.user_id);
	crud.data1("job_id,job_company,job_position,job_period_from,job_period_to,job_current","job",`job_current ='false' AND job_userid='${req.body.user_id}' ORDER BY job_id DESC`,function(error,result){
		//console.log(result);
		if(!error)
		{
			//res.send(result);
			if(result.length > 0)
			{
				res.send(result);	
			}	
			else
			{
				//console.log("Data not found");
				res.send("no");
			}
		}	

	})

});
/*Add users skills*/
router.post('/adduserEducation',(req,res)=>{
	//res.send('getSkill');
	console.log(req.body);
	id=req.body;
	//console.log(id);
	crud.data2("education","edu_userid,edu_univesity,edu_subject,edu_graduation,edu_year",`'${req.body.userid}','${req.body.university}','${req.body.subject}','${req.body.graduation}','${req.body.year}'`,function(error,result){
		//console.log(result);
			if(!error)
			{
				profile_percentage(req.body.userid,'7');
				res.send(result);
			}	
	});

});

/*Add users skills*/
router.post('/getuserEducation',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	crud.data1("edu_userid,edu_univesity,edu_subject,edu_graduation,edu_year","education",`edu_userid='${req.body.user_id}' ORDER BY edu_id DESC LIMIT 1`,function(error,result){
		//console.log(result);
		if(!error)
		{

			res.send(result);
		}	

	})

});

router.post('/adduserRating',(req,res,next)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	//console.log(id);
	crud.data2("rating","rating_userid,rating_pro,rating_qual,rating_exp,rating_area,rating_avt",`'${req.body.rating_userid}','${req.body.rating_pro}','${req.body.rating_qual}','${req.body.rating_exp}','${req.body.rating_area}','${req.body.rating_avt}'`,function(error,result){
		//console.log(result);
			if(!error)
			{
				//callback(null,results);
				profile_percentage(req.body.rating_userid,'1');
				res.send('Add');
			}	
			else
			{
				res.send('Add');
			}	
	});

});

router.get('/compaireData',(req,res,next)=>{
	crud.data1("user_id,user_name,rating_area,rating_avt,job_company,job_position,user_profileimg","users,rating,job","user_id=rating_userid AND user_id=job_userid AND job_current='true' GROUP BY user_id",function(error,result){
		///console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})

});

router.post('/useTotExp',(req,res,next)=>{

	crud.data1("*","rating",`rating_userid='${req.body.user_id}' LIMIT 1`,function(error,result){
		//console.log(result);
		if(!error)
		{
			if(result.length > 0)
			{
				res.send(result);
			}	
			else
			{
				res.send("0");
			}
		}	

	})

});

/*Get user profile percentage*/
function profile_percentage(userid,type)
{
	crud.data1("profile_userid","profile_score,score_master",`profile_userid='${userid}' AND profile_masterid=${type} `,function(error,result){
		//console.log(result);
		if(!error)
		{
			//console.log(result);
			if(result.length > 0)
			{
				//res.send(result);

			}	
			else
			{
				//res.send("0");
					crud.data2("profile_score","profile_userid,profile_masterid",`'${userid}','${type}'`,function(error1,result1){
					if(!error1)
					{
						//res.send(result1);
						//res.status(200);
					}	
					else
					{
						console.log(error1);
					}	
				});
			}
		}	

	})



}

router.post('/profilePercentage',(req,res,next)=>{

	crud.data1("sum(score_master.score_per) as tot","profile_score,score_master",`profile_userid='${req.body.user_id}' AND profile_masterid=score_id `,function(error,result){
		console.log(result);
		if(!error)
		{
			if(result.length > 0)
			{
				res.send(result);
			}	
			else
			{
				res.send("0");
			}
		}	

	})

});

router.post('/updateSalary',(req,res)=>{
	//res.send('getSkill');
	console.log(req.body);
	id=req.body;
	//str=`select * from eshoper_user where email='${req.body.eshop_email}'`;
	str=`UPDATE users SET user_salary='${req.body.user_salary}' WHERE user_id='${req.body.user_id}'`;
	config.query(str, function (error, results) {
					  	if(!error)
					  	{
					  		res.send("salary update");
					  		profile_percentage(req.body.user_id,'5')
					  	}
					  	else
					  	{
					  		// callback(error,null);
					  		res.send("User add");
					  		//console.log(error)
					  	}	
					}); 

});

router.post('/checkPassword',(req,res)=>{
	console.log(req.body);
	crud.data1("user_id,user_name,user_email,user_pass","users",`user_id='${req.body.user_id}'`,function(error,result)
	{
		//console.log(result[0].user_pass);
		 dbpass =result[0].user_pass;
		// console.log(dbpass);
		bcrypt.hash(req.body.user_oldpass, saltRounds, function(err, hash) {
			userpass=req.body.user_oldpass;

						bcrypt.compare(userpass, dbpass, function(err, result_from_crypt) {
							//res.send("1");
							console.log(result_from_crypt);
							// console.log(result_from_crypt);
							if(result_from_crypt==true)
							{
							
								res.status(200).send(JSON.stringify("Valid Password"));
							}	
							else
							{
								res.status(401).send(JSON.stringify("Invalid Password"));
							
							}
						});	
		})
						
	// bcrypt.hash(req.body.user_oldpass,saltRounds,function(err,hash){
	// 	console.log(hash);
	// })
	});
})


router.post('/updatePassword',(req,res)=>{
	//res.send('getSkill');
	//console.log(req.body);
	id=req.body;
	bcrypt.hash(req.body.user_pass, saltRounds, function(err, hash) {
					// Store hash in your password DB.
					if(!err){
						 str=`UPDATE users SET user_pass='${hash}' WHERE user_id='${req.body.user_id}'`;
							config.query(str, function (error, results) {
											  	if(!error)
											  	{
											  		// console.log(results);
											  		res.send(results)
											  		// callback(null,results);
											  	}
											  	else
											  	{
											  		// callback(error,null);
											  		// res.send("User add");
											  		console.log(error)
											  	}	
											}); 

					}
				  });
	

});
router.get("/node_category",function(req,res){
    
	config.query("select * from category", function (error, results) {
					  	if(!error)
					  	{
					  		// console.log(results);
					  		res.send(results)
					  		// callback(null,results);
					  	}
					  	else
					  	{
					  		// callback(error,null);
					  		// res.send("User add");
					  		res.send(error)
					  	}	
					}); 
})

router.get("/logout",function(req,res){
	delete token;
	jwt.sign({
		        data: {uid:"",email:"",status:0,fullname:""}
		      }, 'hellowrold', { expiresIn: 0 });
			//console.log(token);
		    res.send(JSON.stringify("done"))
})
/*admin register no of users*/
router.post("/registerd_user_count",function(req,res,next){
	console.log(req.body)
	id=req.body;
	console.log(id);
	crud.data1("count(*) as cnt,user_id,user_name,user_email,user_salary","users",`user_type='0'`,function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})
});
router.post("/registerd_user_list",function(req,res,next){
	console.log(req.body)
	id=req.body;
	console.log(id);
	crud.data1("user_id,user_name,user_email,user_salary,user_bob","users",`user_type='0'`,function(error,result){
		//console.log(result);
		if(!error)
		{
			res.send(result);
		}	

	})
});
module.exports=router